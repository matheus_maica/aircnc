package com.sicredi.AirCnc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirCncApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirCncApplication.class, args);
	}

}
