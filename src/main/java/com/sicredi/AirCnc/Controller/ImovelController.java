package com.sicredi.AirCnc.Controller;

import com.sicredi.AirCnc.Model.Imovel;
import com.sicredi.AirCnc.Service.ImovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/imovel")
public class ImovelController {
    @Autowired
    private ImovelService imovelService;

    @GetMapping
    public List<Imovel> listarImoveis(){
        return imovelService.listarImoveis();
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<Imovel> cadastrarImovel(@RequestBody Imovel imovel){
       Imovel auxImovel= imovelService.cadastrarImovel(imovel);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxImovel);
    }
}
