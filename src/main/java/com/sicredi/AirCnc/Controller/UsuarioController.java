package com.sicredi.AirCnc.Controller;

import com.sicredi.AirCnc.DTO.UsuarioCadastroDTO;
import com.sicredi.AirCnc.DTO.UsuarioDTO;
import com.sicredi.AirCnc.Model.Usuario;
import com.sicredi.AirCnc.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public List<Usuario> listarUsuarios(){
        return usuarioService.findAll();
    }

    @PostMapping("/adicionar")
    public ResponseEntity<UsuarioDTO> adicionarUsuario(@RequestBody UsuarioCadastroDTO newUser){
        UsuarioDTO auxUser = usuarioService.addUsuario(newUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxUser);
    }

    @PutMapping("/editar/{nome}")
    public ResponseEntity<UsuarioDTO> editarUsuario(@PathVariable String nome, @RequestBody Usuario usuario){
        UsuarioDTO auxUser = usuarioService.editUser(nome, usuario);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(auxUser);
    }

    @DeleteMapping("/deletar/{nome}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deletarUsuario(@PathVariable String nome){
        usuarioService.deletUser(nome);
    }
}
