package com.sicredi.AirCnc.DTO;

import com.sicredi.AirCnc.Util.UsuarioTipo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioCadastroDTO {
    private String nome;
    private String senha;
    private UsuarioTipo usuarioTipo;
}
