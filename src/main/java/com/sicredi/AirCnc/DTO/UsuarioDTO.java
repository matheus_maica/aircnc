package com.sicredi.AirCnc.DTO;

import com.sicredi.AirCnc.Model.Usuario;
import com.sicredi.AirCnc.Util.UsuarioTipo;
import lombok.Data;

@Data
public class UsuarioDTO {
    private String nome;
    private UsuarioTipo usuarioTipo;

    public UsuarioDTO(Usuario usuario){
        nome = usuario.getNome();
        usuarioTipo = usuario.getUsuarioTipo();
    }
}
