package com.sicredi.AirCnc.Model;

import com.sicredi.AirCnc.Util.ImovelTipo;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Document(collection = "imoveis")
public class Imovel {
    private String _id;
    private String descricao;
    private String proprietario;
    private String locatario;
    private BigDecimal valorDiaria;
    private ImovelTipo tipo;
    private Boolean disponivel;
}
