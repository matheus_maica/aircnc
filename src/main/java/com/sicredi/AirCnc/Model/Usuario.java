package com.sicredi.AirCnc.Model;

import com.sicredi.AirCnc.Util.UsuarioTipo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "usuarios")
public class Usuario {
    @Id
    private String nome;
    private String senha;
    private UsuarioTipo usuarioTipo;
    private List<Imovel> imoveis;

    public void addImovel(Imovel imovel){
        imoveis.add(imovel);
    }

    public Boolean removeImoveis(Imovel imovel){
        for(Imovel imovel1: imoveis){
            if(Objects.equals(imovel1.get_id(),imovel.get_id())){
                imoveis.remove(imovel1);
                return true;
            }
        }
        return false;
    }

}
