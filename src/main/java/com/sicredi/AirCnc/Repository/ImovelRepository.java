package com.sicredi.AirCnc.Repository;

import com.sicredi.AirCnc.Model.Imovel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImovelRepository extends MongoRepository<Imovel, String> {
}
