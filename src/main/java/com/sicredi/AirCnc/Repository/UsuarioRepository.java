package com.sicredi.AirCnc.Repository;

import com.sicredi.AirCnc.Model.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsuarioRepository extends MongoRepository<Usuario,String> {
}
