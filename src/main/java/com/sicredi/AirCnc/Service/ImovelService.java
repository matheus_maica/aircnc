package com.sicredi.AirCnc.Service;

import com.sicredi.AirCnc.Model.Imovel;
import com.sicredi.AirCnc.Repository.ImovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImovelService {
    @Autowired
    private ImovelRepository imovelRepository;

    public List<Imovel> listarImoveis() {
        return imovelRepository.findAll();
    }

    public Imovel cadastrarImovel(Imovel imovel) {
        return imovelRepository.insert(imovel);
    }
}
