package com.sicredi.AirCnc.Service;

import com.sicredi.AirCnc.DTO.UsuarioCadastroDTO;
import com.sicredi.AirCnc.DTO.UsuarioDTO;
import com.sicredi.AirCnc.Model.Imovel;
import com.sicredi.AirCnc.Model.Usuario;
import com.sicredi.AirCnc.Repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> findAll(){
        return usuarioRepository.findAll();
    }

    public UsuarioDTO addUsuario(UsuarioCadastroDTO usuario) {
        Usuario usModel = new Usuario(usuario.getNome(),usuario.getSenha(),usuario.getUsuarioTipo(),
                new ArrayList<Imovel>());
        Usuario auxUser = usuarioRepository.save(usModel);
        return new UsuarioDTO(auxUser);
    }

    public Usuario validarUsuario(String nome){
        Optional<Usuario> usuario = usuarioRepository.findById(nome);
        if(usuario.isEmpty()){
            throw new IllegalArgumentException();
        }
        return usuario.get();
    }

    public UsuarioDTO editUser(String nome, Usuario usuario) {
        Usuario auxUser = validarUsuario(nome);
        BeanUtils.copyProperties(usuario,auxUser);
        usuarioRepository.save(auxUser);
        System.out.println(auxUser.getNome());
        System.out.println(auxUser.getSenha());
        return new UsuarioDTO(auxUser);
    }

    public void deletUser(String nome) {
        Usuario usuario = validarUsuario(nome);
        usuarioRepository.delete(usuario);
    }
}
